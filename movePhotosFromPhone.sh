# Phone connection protocol is MTP
# https://askubuntu.com/questions/596441/how-to-access-my-android-phone-from-my-terminal

fromPath=/run/user/1000/gvfs/mtp:host=OnePlus_OnePlus_06d86625/Internal\ shared\ storage/DCIM/Camera/
toPath=/media/rachelwil/RachelsSSD/BACKUP/ARCHIVE-PHOTOS/CameraPull

echo "FROM: $fromPath"
echo "TO:   $toPath"
echo ""

# Create the path if it doesn't already exist
if [ -d $toPath ]
then
  echo "$toPath already exists!"
else
  echo "Created path $toPath"
  mkdir $toPath
fi

echo ""
echo "RUN: \"mv -v "$fromPath" "$toPath"\""
mv -v "$fromPath" "$toPath"
